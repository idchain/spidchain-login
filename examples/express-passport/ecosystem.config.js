module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    // First application
    {
      name: 'spidchain-login',
      script: 'server/main.js',
      env_production: {
        NODE_ENV: 'production',
        PORT: 80,
        ipfsRoot: 'http://95.85.40.97',
        network: 'testnet'
      }
    }
  ],
  'deploy': {
    'production': {
      'user': 'root',
      // Multi host is possible, just by passing IPs/hostname as an array
      'host': ['37.139.13.234'],
      // Branch
      'ref': 'origin/master',
      // Git repository to clone
      'repo': 'https://bitbucket.org/idchain/spidchain-login',
      // Path of the application on target servers
      'path': '/opt/spidchain-login',
      // Commands to be executed on the server after the repo has been cloned
      'post-deploy': 'npm install && \
                      cd examples/express-passport && \
                      npm install && \
                      pm2 startOrRestart ecosystem.config.js --env production',
      // Environment variables that must be injected in all applications on this env
      'env': {
        'NODE_ENV': 'production'
      }
    }
  }
}
