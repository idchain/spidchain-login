const PORT = process.env.PORT || 3000
if (!PORT) throw Error('PORT is undefined')
const path = require('path')
const express = require('express')
const cors = require('cors')
const session = require('express-session')
const {json} = require('body-parser')
const _ = require('lodash')
const mustacheExpress = require('mustache-express')
const qr = require('qr-image')
const passport = require('passport')
const querystring = require('querystring')
const uuid = require('uuid')
const {URL} = require('url')
const {getDid, getNonce, verifyClaim} = require('../../../lib/index.js')

const users = []

passport.serializeUser((user, done) => {
  done(null, user.id)
})

passport.deserializeUser((id, done) => {
  const user = _.find(users, e => e.id === id)
  if (user) {
    done(null, user)
  }
})

const app = express()
const sessionStore = new session.MemoryStore()
app.use(json())
app.engine('mustache', mustacheExpress())
app.set('view engine', 'mustache')
app.set('layout', 'main')
app.set('views', path.join(__dirname, '../views'))
// Serve the spidchain icon
app.use(express.static('public'))

app.get('/qrcode/:path', (req, res) => {
  const qrSvg = qr.image(decodeURIComponent(req.params.path), {type: 'svg'})
  res.setHeader('Content-type', 'image/svg+xml')
  // code.pipe(res);
  qrSvg.pipe(res)
})

app.options('/authorize', cors())

app.post('/authorize', cors(), async (req, res) => {
  const {loginClaim, otherClaims} = req.body
  // TODO: improv error handling
  if (!loginClaim) {
    console.error('missing params')
    res.sendStatus(404)
    return
  }
  const url = new URL(getNonce(loginClaim))
  const sid = url.searchParams.get('sid')
  const signup = url.searchParams.get('signup') === 'true'
  try {
    const verified = await verifyClaim(loginClaim)
    if (!verified) {
      console.error('wrong signature')
      res.sendStatus(404)
      return
    }
  } catch (e) {
    console.error(e)
    return res.sendStatus(500)
  }

  // TODO check if user already exists, if true push the new nonce in the session
  const did = getDid(loginClaim)

  sessionStore.get(sid, async (error, session) => {
    if (error) {
      console.error(error)
      return
    }
    if (session) {
      // Don't log in is session is already logged in
      if (session.passport && session.passport.user) {
        res.sendStatus(404)
        return
      }

      // If user with given did does not exist in db, create one
      let newUser = false
      let user = _.find(users, e => e.did === did)
      if (!user) {
        // Fail if the user is not signing up
        if (!signup) {
          res.sendStatus(404)
          return
        }
        const nameClaim = otherClaims[0]
        // TODO verify with correct DID
        try {
          if (!await verifyClaim(nameClaim)) {
            res.sendStatus(401)
            return
          }
        } catch (e) {
          console.error(e)
          return res.sendStatus(500)
        }
        const {familyName, givenName, email} = nameClaim
        user = {id: uuid(), did, familyName, givenName, email}
        newUser = true
      }
      sessionStore.set(sid, {...session, passport: {user: user.id}}, (err) => {
        if (err) {
          console.error(err)
          res.sendStatus(500)
          return
        }
        // Add new user to db
        if (newUser) {
          users.push(user)
        }
        res.sendStatus(204)
      })
    }
  })
})

app.use(session({
  store: sessionStore,
  secret: 'spidchain',
  resave: false,
  saveUninitialized: true,
  // TODO: secure: true only works with https connection
  cookie: { secure: false }
}))
app.use(passport.initialize())
app.use(passport.session())

app.get('/', (req, res) => {
  const sid = req.session.id
  const user = req.user
  const authorizeUrl = `${req.protocol}://${req.get('host')}/authorize?`
  const loginPath = authorizeUrl + querystring.stringify({sid})
  const signupPath = authorizeUrl + querystring.stringify({sid, signup: true})
  res.render('home', {
    user,
    loginPath,
    loginPathEncoded: encodeURIComponent(loginPath),
    signupPath,
    signupPathEncoded: encodeURIComponent(signupPath)
  })
})

app.get('/restricted', (req, res) => {
  const user = req.user
  const sid = req.session.id
  const authorizeUrl = `${req.protocol}://${req.get('host')}/authorize?`
  const loginPath = authorizeUrl + querystring.stringify({sid})
  const signupPath = authorizeUrl + querystring.stringify({sid, signup: true})
  res.render('restricted', {
    user,
    loginPath,
    loginPathEncoded: encodeURIComponent(loginPath),
    signupPath,
    signupPathEncoded: encodeURIComponent(signupPath)
  })
})

app.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
})

app.listen(PORT)
