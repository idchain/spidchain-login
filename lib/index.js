'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var bitcoreMessage = _interopDefault(require('bitcore-message'));
var jsigs = _interopDefault(require('jsonld-signatures'));
var jsonld = _interopDefault(require('jsonld'));
var bitcoin = require('bitcoinjs-lib');
var bitcoin__default = _interopDefault(bitcoin);
var _ = _interopDefault(require('lodash'));
var txrefConversionJs = require('txref-conversion-js');
var axios = _interopDefault(require('axios'));

const corsProxyUrl = process.env.corsProxyUrl || '';

const blockcypherApi = process.env.network === 'testnet' ? corsProxyUrl + 'https://api.blockcypher.com/v1/btc/test3/' : corsProxyUrl + 'https://api.blockcypher.com/v1/btc/main/';

const soChainApi = process.env.network === 'testnet' ? method => corsProxyUrl + `https://chain.so/api/v2/${method}/BTCTEST/` : method => corsProxyUrl + `https://chain.so/api/v2/${method}/BTC/`;

const getSpendingTx = async (txId, ix) => {
  // block cypher
  let blockcypherRes;
  try {
    const { status: blockcypherStatus, data: cyData } = await axios.get(blockcypherApi + `/txs/${txId}`);
    blockcypherRes = blockcypherStatus !== 200 || cyData.error ? { failed: true } : cyData.outputs[ix].spent_by ? cyData.outputs[ix].spent_by : null;
  } catch (e) {
    blockcypherRes = { failed: true, message: e.message };
  }
  // sochain
  let sochainRes;
  const sochainUrl = soChainApi('is_tx_spent') + txId + '/' + ix;
  try {
    const { status: soStatus, data: soData } = await axios.get(sochainUrl);
    sochainRes = soStatus !== 200 || soData.status === 'fail' ? { failed: true } : soData.data.is_spent ? soData.data.spent.txid : null;
    //  soData.spent.input_no
  } catch (e) {
    sochainRes = { failed: true, message: e.message };
  }
  const result = _.filter([sochainRes, blockcypherRes], e => e === null || !e.failed);
  if (_.isEmpty(result)) {
    throw new Error('No results from block-explorers');
  }
  const firstTxId = _.head(result);
  if (!_.every(result, e => e === firstTxId)) {
    throw new Error('Block-explorers results mismatch');
  }
  return firstTxId;
};



const getTxInfo = async txId => {
  let blockcypherRes;
  try {
    const {
      status: blockcypherStatus,
      data: { error, block_hash, block_height, block_index, confirmed, hex }
    } = await axios.get(blockcypherApi + `/txs/${txId}`, {
      params: {
        includeHex: true
      }
    });
    blockcypherRes = blockcypherStatus !== 200 || error ? { failed: true } : {
      blockHash: block_hash,
      blockTime: new Date(confirmed),
      height: block_height,
      ix: block_index,
      tx: hex
    };
  } catch (e) {
    blockcypherRes = { failed: true, message: e.message };
  }
  let sochainRes;
  const sochainUrl = soChainApi('get_tx') + txId;
  try {
    const { status: soStatus, data: soData } = await axios.get(sochainUrl);
    sochainRes = soStatus !== 200 || soData.status !== 'success' ? { failed: true } : { tx: soData.data.tx_hex,
      blockTime: new Date(soData.data.time * 1000),
      blockHash: soData.data.blockhash };
    if (!sochainRes.failed) {
      const sochainUrl2 = soChainApi('get_block') + sochainRes.blockHash;
      const { status: soStatus2, data: soData2 } = await axios.get(sochainUrl2);
      sochainRes = soStatus2 !== 200 || soData2.status !== 'success' ? { failed: true } : Object.assign({}, sochainRes, {
        ix: _.findIndex(soData2.data.txs, t => t === txId),
        height: soData2.data.block_no
      });
    }
  } catch (e) {
    sochainRes = { failed: true, message: e.message };
  }
  const result = _.filter([sochainRes, blockcypherRes], e => !e.failed);
  if (_.isEmpty(result)) {
    throw new Error('No results from the explorers');
  }
  const firstRes = _.head(result);
  if (!_.every(result, e => _.isEqual(e, firstRes))) {
    throw new Error('Block-explorers results mismatch');
  }
  return firstRes;
};

const getPath = async txId => {
  const path = [];
  let thisTx = txId;
  do {
    const txInfo = await getTxInfo(thisTx);
    path.push(txInfo);
    thisTx = await getSpendingTx(thisTx, 0);
  } while (thisTx);
  return path;
};

const txrefToTxid = async (height, index) => {
  let blockcypherRes;
  try {
    const { status: blockcypherStatus, data: { error, txids } } = await axios.get(blockcypherApi + `/blocks/${height}?txstart=${index}&limit=1`);
    blockcypherRes = blockcypherStatus !== 200 || error ? { failed: true } : txids[0];
  } catch (e) {
    blockcypherRes = { failed: true, message: e.message };
  }
  let sochainRes;
  try {
    const sochainUrl = soChainApi('get_block') + height;
    const { status: soStatus, data: soData } = await axios.get(sochainUrl);
    sochainRes = soStatus !== 200 || soData.status !== 'success' ? { failed: true } : soData.data.txs[index];
  } catch (e) {
    sochainRes = { failed: true, message: e.message };
  }
  const result = _.filter([sochainRes, blockcypherRes], e => !e.failed);
  if (_.isEmpty(result)) {
    throw new Error('No results from the explorers');
  }
  const firstRes = _.head(result);
  if (!_.every(result, e => e === firstRes)) {
    throw new Error('Block-explorers results mismatch');
  }
  return firstRes;
};

const getDeterministicDDO = tx => {
  const network = bitcoin__default.networks[process.env.network];
  const inputScript = tx.ins[0].script;
  const [{ script: conScript }, { script: nullData }, { script: recScript }] = tx.outs;
  const ownerPubKey = bitcoin__default.script.pubKeyHash.input.decode(inputScript).pubKey.toString('hex');
  const conAddress = bitcoin__default.address.fromOutputScript(conScript, network);
  const recAddress = bitcoin__default.address.fromOutputScript(recScript, network);
  const extendedDDOUrl = bitcoin__default.script.nullData.output.decode(nullData).toString();
  return { ownerPubKey, conAddress, extendedDDOUrl, recAddress };
};

const apiUrlGet = process.env.ipfsRoot + '/api/ipfs/get';


const ipfsGet = async hash => {
  const { data, status } = await axios.post(apiUrlGet, { hash });
  if (status !== 200) {
    console.error('there was an error');
    throw Error(data);
  }
  return data;
};

// This should be generic, checking the type of URL (ipfs, github, drive...)
// and does the appropriet get request
const getExtendedDDO = async extendedDDOUrl => {
  const extendedDDOJSON = await ipfsGet(extendedDDOUrl);
  return JSON.parse(extendedDDOJSON);
};

const isTxId = str => /^[0-9A-Fa-f]{64}$/.test(str);

const getDDO = async DID => {
  let txId;

  if (isTxId(DID)) {
    txId = DID;
  } else {
    const { blockHeight, blockIndex } = txrefConversionJs.txrefDecode(DID);
    txId = await txrefToTxid(blockHeight, blockIndex);
  }

  const TxPath = await getPath(txId);
  const lastTx = _.last(TxPath);
  const tx = bitcoin__default.Transaction.fromHex(lastTx.tx);
  const deterministicDDO = getDeterministicDDO(tx);
  const { extendedDDOUrl } = deterministicDDO;
  const extendedDDO = await getExtendedDDO(extendedDDOUrl);
  return { deterministicDDO, extendedDDO };
};

// TODO: find a better way to import bitcore-message
jsigs.use('bitcoreMessage', bitcoreMessage);
jsigs.use('jsonld', jsonld);

const network = bitcoin.networks[process.env.network];

// TODO Put back keyId in the arguments list
const verifyClaim$1 = async ({ signedDocument, signerDid }) => {
  const { deterministicDDO: { ownerPubKey } } = await getDDO(signerDid);
  const keyPair = bitcoin.ECPair.fromPublicKeyBuffer(Buffer.from(ownerPubKey, 'hex'), network);
  const publicKeyWif = keyPair.getAddress();
  const verifiedSigs = await jsigs.promises().verify(signedDocument, {
    publicKey: {
      '@context': jsigs.SECURITY_CONTEXT_URL,
      //   id: keyId,
      id: 'did:btcr:' + signerDid,
      type: 'CryptographicKey',
      owner: 'did:btcr:' + signerDid,
      publicKeyWif
    },
    publicKeyOwner: {
      '@context': jsigs.SECURITY_CONTEXT_URL,
      id: 'did:btcr:' + signerDid,
      // publicKey: [keyId]
      publicKey: ['did:btcr:' + signerDid]
    }
  });
  const found = _.find(verifiedSigs.keyResults, { publicKey: 'did:btcr:' + signerDid });
  return found ? found.verified : false;
};

const getDid = claim => {
  const signature = claim['https://w3id.org/security#signature'];
  if (Array.isArray(signature)) {
    return signature[0]['http://purl.org/dc/terms/creator']['@id'];
  }
  return signature['http://purl.org/dc/terms/creator']['@id'];
};

const getNonce = claim => {
  return claim['http://schema.org/url'];
};

const verifyClaim = async claim => {
  const did = getDid(claim).split(':')[2];
  return verifyClaim$1({ signedDocument: claim, signerDid: did });
};

exports.getDid = getDid;
exports.getNonce = getNonce;
exports.verifyClaim = verifyClaim;
