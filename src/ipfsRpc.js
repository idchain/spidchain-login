import axios from 'axios'

const apiUrlGet = process.env.ipfsRoot + '/api/ipfs/get'
const apiUrlAdd = process.env.ipfsRoot + '/api/ipfs/add'

export const ipfsAdd = async (text) => {
  const {data, status} = await axios.post(apiUrlAdd, {text})
  if (status !== 200) {
    console.error('there was an error')
    throw Error(data)
  }
  return data
}

export const ipfsGet = async (hash) => {
  const {data, status} = await axios.post(apiUrlGet, {hash})
  if (status !== 200) {
    console.error('there was an error')
    throw Error(data)
  }
  return data
}
