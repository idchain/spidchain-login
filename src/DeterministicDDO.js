import bitcoin from 'bitcoinjs-lib'

const getDeterministicDDO = (tx) => {
  const network = bitcoin.networks[process.env.network]
  const inputScript = tx.ins[0].script
  const [{script: conScript}, {script: nullData}, {script: recScript}] = tx.outs
  const ownerPubKey = bitcoin.script.pubKeyHash.input.decode(inputScript).pubKey.toString('hex')
  const conAddress = bitcoin.address.fromOutputScript(conScript, network)
  const recAddress = bitcoin.address.fromOutputScript(recScript, network)
  const extendedDDOUrl = bitcoin.script.nullData.output.decode(nullData).toString()
  return {ownerPubKey, conAddress, extendedDDOUrl, recAddress}
}

export default getDeterministicDDO
