import {txrefDecode} from 'txref-conversion-js'
import _ from 'lodash'
import bitcoin from 'bitcoinjs-lib'

import {getPath, txrefToTxid} from './txUtils'
import getDeterministicDDO from './DeterministicDDO'
import {getExtendedDDO} from './ExtendedDDO'

const isTxId = (str) => /^[0-9A-Fa-f]{64}$/.test(str)

const getDDO = async (DID) => {
  let txId

  if (isTxId(DID)) {
    txId = DID
  } else {
    const {blockHeight, blockIndex} = txrefDecode(DID)
    txId = (await txrefToTxid(blockHeight, blockIndex))
  }

  const TxPath = await getPath(txId)
  const lastTx = _.last(TxPath)
  const tx = bitcoin.Transaction.fromHex(lastTx.tx)
  const deterministicDDO = getDeterministicDDO(tx)
  const {extendedDDOUrl} = deterministicDDO
  const extendedDDO = await getExtendedDDO(extendedDDOUrl)
  return {deterministicDDO, extendedDDO}
}

export default getDDO
