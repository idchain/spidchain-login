import {crypto} from 'bitcoinjs-lib'

import {ipfsAdd, ipfsGet} from './ipfsRpc'

const sha256 = msg => crypto.sha256(msg)

// This should be generic, checking the type of URL (ipfs, github, drive...)
// and does the appropriet get request
export const getExtendedDDO = async (extendedDDOUrl) => {
  const extendedDDOJSON = await ipfsGet(extendedDDOUrl)
  return JSON.parse(extendedDDOJSON)
}

export const makeExtendedDDO = async ({DID, claimsKeyPair, ownerKeyPair}) => {
  // const controlKeyPair = controlRoot.derive(0)
  // const claimsKeyPair = claimsRoot.derive(0)
  // const claimsPubKeys = [claimsKeyPair.getPublicKeyBuffer().toString('hex')]
  const ExtendedDDO = assembleExtendedDDO({DID, ownerKeyPair, claimsKeyPair})
  const extendedDDOJSON = JSON.stringify(ExtendedDDO)
  const [{hash}] = await ipfsAdd(extendedDDOJSON)
  return hash
}

const makeDID = (txId) => {
  return `did:btcr:${txId}`
}

const assembleExtendedDDO = ({DID, ownerKeyPair, claimsKeyPair}) => {
  const did = makeDID(DID)
  const owner = {
    id: `${did}#key-1`,
    type: ['CryptographicKey', 'EcDsaPublicKey'],
    curve: 'secp256k1',
    publicKey: claimsKeyPair.getPublicKeyBuffer().toString('hex')
  }
  const timeStamp = new Date().toDateString()
  const ddo = {
    '@context': [
      'https://schema.org/',
      'https://w3id.org/security/v1'
    ],
    issuer: did,
    issued: timeStamp,
    type: [
      'Credential',
      'Identity',
      'Person'
    ],
    owner
  }
  const ddoJSON = JSON.stringify(ddo)
  const ddoHash = sha256(ddoJSON)
  const ddoSignature = ownerKeyPair.sign(ddoHash).toDER().toString('hex')
  return {
    ...ddo,
    ddoSignature
  }
}
