import verify from './src/verifyClaim'

export const getDid = (claim) => {
  const signature = claim['https://w3id.org/security#signature']
  if (Array.isArray(signature)) {
    return signature[0]['http://purl.org/dc/terms/creator']['@id']
  }
  return signature['http://purl.org/dc/terms/creator']['@id']
}

export const getNonce = (claim) => {
  return claim['http://schema.org/url']
}

export const verifyClaim = async (claim) => {
  const did = getDid(claim).split(':')[2]
  return verify({signedDocument: claim, signerDid: did})
}
