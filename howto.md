How to integrate SpidChain login system using Express and Passport
==================================================================
Integrating SpidChain login system in a website allow users to signup and login
with one click, without using a centralised system and providing certified
identities.

First install the library:

    npm install --save spidchain-login

There are only three necessary functions `getDid`, `getNonce`, `verifyClaim`:

    const {getDid, getNonce, verifyClaim} = require('spidchain-login')

Let's see how to integrate it in the [passportjs](http://passportjs.org/)
authentication middleware.

There should be a route `/authorize` to which the app will do a POST request.
Inside the body of the POST there is a signed claim containing the session id
and the DID of the signer. And an array of other claims regarding users
attributes.

Note that this route should appear before the part where the session middleware
is defined, otherwise each time the app connects to authorize a useless session
will be created.

    app.post('/authorize', async (req, res) => {

The body of the post will contain a claim with the session id signed
and an array of other claims sent by the user

      const {loginClaim, otherClaims} = req.body

If no login claim abort authorization

      if (!loginClaim) {
        console.error('missing params')
        res.sendStatus(404)
        return
      }

Extract the session Id from the login claim

      const url = new URL(getNonce(loginClaim))

Check if the user already exists

      const sid = url.searchParams.get('sid')

Check if the app is doing a signup

      const signup = url.searchParams.get('signup') === 'true'

Now verify the signature of the claim containing the session id

      try {

verifyClaim will use the blockchain that the signature of the claim
is valid

        const verified = await verifyClaim(loginClaim)
        if (!verified) {
          console.error('wrong signature')
          res.sendStatus(404)
          return
        }
      } catch (e) {
        console.error(e)
        return res.sendStatus(500)
      }

Extract the DID from the claim that has been sent

    const did = getDid(loginClaim)

Get the session object corresponding to the given session id

    sessionStore.get(sid, async (error, session) => {
    if (error) {
      console.error(error)
      return
    }
    if (session) {

Don't log in if user is already logged in

      if (session.passport && session.passport.user) {
        res.sendStatus(404)
        return
      }


      let newUser = false

If user with given did does not exist in db, create one

      let user = _.find(users, e => e.did === did)
      if (!user) {

Fail if the user is not signing up

        if (!signup) {
          res.sendStatus(404)
          return
        }

Take the first of the remaining claims

        const nameClaim = otherClaims[0]

verify signature

        try {
          if (!await verifyClaim(nameClaim)) {
            res.sendStatus(401)
            return
          }
        } catch (e) {
          console.error(e)
          return res.sendStatus(500)
        }

extract the attributes from the claim and assemble a user

        const {familyName, givenName, email} = nameClaim
        user = {id: uuid(), did, familyName, givenName, email}
        newUser = true
      }

store the user in the session object

      sessionStore.set(sid, {...session, passport: {user: user.id}}, (err) => {
        if (err) {
          console.error(err)
          res.sendStatus(500)
          return
        }

Add new user to db, in this case push it to an in-memory array

        if (newUser) {
          users.push(user)
        }
        res.sendStatus(204)
        })
       }
      })
    })


Now there should be all the classic Session and Passport machinery.

Generate a session:

    app.use(session({
      store: sessionStore,
      secret: 'spidchain',
      resave: false,
      saveUninitialized: true,
      // This should be set to `true` in production to only use https
      cookie: { secure: false }
    }))

Initialize an use Passport:

    app.use(passport.initialize())
    app.use(passport.session())

Define a root route where we pass the session id which will be shown in the QRCode used to login:

    app.get('/', (req, res) => {
      const sid = req.session.id
      const user = req.user
      // This is the urs that the app will post to. It should be shown in a QRCode
      const authorizeUrl = `${req.protocol}://${req.get('host')}/authorize?`
      const loginPath = authorizeUrl + querystring.stringify({sid})
      const signupPath = authorizeUrl + querystring.stringify({sid, signup: true})
      res.render('home', {
        user,
        loginPath,
        loginPathEncoded: encodeURIComponent(loginPath),
        signupPath,
        signupPathEncoded: encodeURIComponent(signupPath)
      })
    })

Now the restricted route. This is standard passport code and extracts the
`user` object attached to `req`. It then passes it to the view that will not
show the restricted area only if its content is not undefined.

    app.get('/restricted', (req, res) => {
      const user = req.user
      const sid = req.session.id
      const authorizeUrl = `${req.protocol}://${req.get('host')}/authorize?`
      const loginPath = authorizeUrl + querystring.stringify({sid})
      const signupPath = authorizeUrl + querystring.stringify({sid, signup: true})
      res.render('restricted', {
        user,
        loginPath,
        loginPathEncoded: encodeURIComponent(loginPath),
        signupPath,
        signupPathEncoded: encodeURIComponent(signupPath)
      })
    })

The full example is in the examples folder.
